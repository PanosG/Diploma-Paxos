To use this piece of software 
1) sudo apt-get install libc-dev-bin g++ libasync
2) For dmalloc lib:
    ./configure && make,
    sudo make install
3) For sfs1 lib:
    ./configure CFLAGS='-D_GNU_SOURCE' CXXFLAGS='-D_GNU_SOURCE -fpermissive -Wno-error' 
    CXXWFLAGS='' --without-sfsuser --without-sfsgroup --with-dmalloc,
    make,
    sudo make install
    
This implementation of Paxos  protocol is based on  David Mazieres' implementation
that is presented in his report entitled: “Paxos Made Practical” 
and some pieces of source published in his course: Distributed Systems (Winter 2007)