#include "rep.h"
#include <sys/time.h>

#define HEARTBEAT_PERIOD 15
#define GRACE_PERIOD 2

class Participant
{
    int health;
	mid_t id;
    struct timeval lease_time;


public:
    Participant(mid_t uid);
    Participant();
    void reincarnate();
    void kill();
    void lease();
    bool alive();
    bool have_lease();
    mid_t get_id();

};