#include <stdlib.h>
#include <pthread.h>
#include "test_client.h"
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <time.h>

int requests_count = 0;
int responses_count = 0;
FILE *results_fp;
storage_t *storage;
pthread_t producer_th[PROD_THREAD_NUM];
pthread_t consumer_th[CONS_THREAD_NUM];
pthread_t writer_th;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t responses_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t queue_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t requests_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t dir_creation_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond_consumer = PTHREAD_COND_INITIALIZER;
pthread_cond_t cond_producer = PTHREAD_COND_INITIALIZER;
pthread_attr_t attr;
static fifo_t *fifo;
int req_id = 0;
int dirs_count = 0;

int prev_resp_count = 0, prev_req_count = 0;
char *line;
int requests_total, responses_total;
double responses_interval, requests_interval;

int op_weighted_rand()
{
	double rand_value = (double)rand() / (double)RAND_MAX;
	if (rand_value <= 0.35)
        return 0; // ls
    /*else if (rand_value > 0.35 && rand_value <= 0.4)
        return 2; // rm*/
    else if (rand_value > 0.35 && rand_value <= 0.55)
        return 5; // mkdir
    else if (rand_value > 0.55 && rand_value <= 0.75)
        return 1; // mkfile
    else if (rand_value > 0.75 && rand_value <= 0.90)
        return 3; // read
    else
		return 4; // write
}

void execute_request_result(request_t *req)
{
	
	if (strcmp(req->op, OP[1]) == 0)
	{
		char *file_name = get_file_name(req);
		dir_append(storage->dirs[req->dir_index], file_name);
	}/*
	else if (strcmp(req->op, OP[2]) == 0)
	{
		char *file_name = get_file_name(req);
		dir_delete(directories[req->dir_index], file_name);
	}*/
	else if(strcmp(req->op,OP[5]) == 0)
	{ 
		pthread_mutex_lock(storage->mutex);
        if(storage->dir_count < DIRS_SIZE)
        {
            int index = ++storage->dir_count;
            storage->dirs[index] = dir_init(req->op_path, index);
        }
        pthread_mutex_unlock(storage->mutex);
	}
	
}

void spawn_process(request_t *req, int id)
{
	pthread_mutex_lock(&requests_mutex);
	requests_count++;
	pthread_mutex_unlock(&requests_mutex);
	pid_t fpid = fork();
	int status;
	if (fpid == 0)
	{
		execute(req);
		_exit(111);
	}
	if(WAIT_FOR_RESPONSE == 1)
	{
		if (fpid > 0)
		{
			if (waitpid(fpid, &status, 0) > 0)
			{
				
				if (WIFEXITED(status) && !WEXITSTATUS(status))
				{
					execute_request_result(req);
				}
				else if (WIFEXITED(status) && WEXITSTATUS(status))
					if (WEXITSTATUS(status) == 111)
						perror("execl");
				pthread_mutex_lock(&responses_mutex);
				responses_count++;
				pthread_mutex_unlock(&responses_mutex);
			
			}
			free(req);
		}
	}
	else
	{
		execute_request_result(req);
	}
	
}

char *generate_random_word(int length)
{
	int word_length = rand() % length + 5;
	char *word = calloc(word_length , sizeof(*word));
	int i;
	for (i = 0; i < word_length; i++)
		word[i] = 'a' + (rand() % 25);
	word[word_length-1] = '\0';
	return word;
}

char *get_random_file(directory_t *dir)
{
	if(dir->paths_size <= 0)
		return generate_random_word(FILE_NAME_SIZE);
	else
		return dir_get_file(dir);
}

directory_t *get_random_directory(request_t *req)
{
    //char *dir;
    int dir_index = 0;
    if (dirs_count > 0)
        dir_index = rand() % dirs_count;
    req->dir_index = dir_index;
    //fprintf(stderr,"%ld %d %d\n", directories[dir_index]->paths_size, dir_index, dirs_count);
    return storage->dirs[dir_index];
}

char *form_request_path(char *dir, char *file, int dir_index)
{
	size_t dir_len = strlen(dir);
	size_t file_len = strlen(file) + 1;
	char *path = NULL;
	if(dir_len > 1)
	{
		int backslash_len = strlen("/");
		path = calloc( dir_len + file_len + backslash_len ,sizeof(*path));
		strcat(path, dir);
		strcat(path, "/");
		strcat(path, file);
	}
	else
	{
		path = calloc( dir_len + file_len , sizeof(*path));
		strcat(path, dir);
		strcat(path, file);
	}
	return path;
}

request_t *generate_random_request()
{
	request_t *request = (request_t *)malloc(sizeof(request_t));
	request->op = NULL;
	request->op_path = NULL;
	request->arg = NULL;
	request->dir_index = -1;
	int request_type = op_weighted_rand();
	char *op = calloc( strlen(OP[request_type]) + 1, sizeof(*op));
	strcpy(op, OP[request_type]);
	request-> op = op;
	if (strcmp(request->op, OP[0]) == 0) //ls
    {
        int dir_index;
        if (dirs_count > 0)
            dir_index = rand() % dirs_count;
        else
            dir_index = dirs_count;
        request->op_path = storage->dirs[dir_index]->dir_name;
    }
    else if (strcmp(request->op, OP[1]) == 0) //mkfile
    {
        char *file_name = generate_random_word(FILE_NAME_SIZE);
        directory_t *dir = get_random_directory(request);
        char *path = form_request_path(dir->dir_name, file_name, request->dir_index);
        request->op_path = path;
    }
    else if (strcmp(request->op, OP[2]) == 0 || strcmp(request->op, OP[3]) == 0) // read/rm
    {
        directory_t *dir = get_random_directory(request);
        if(dir->paths_size <= 0)
        {
            char *file_name = generate_random_word(FILE_NAME_SIZE);
            char *path = form_request_path(dir->dir_name, file_name, request->dir_index);
            strcpy(request->op, OP[1]);
            request->op_path = path;
        }
        else
        {
            char *file = get_random_file(storage->dirs[request->dir_index]);
            char *path = form_request_path(dir->dir_name, file, request->dir_index);
            request->op_path = path;
        }
    }
    else if (strcmp(request->op, OP[4]) == 0) //write
    {
        directory_t *dir = get_random_directory(request);
        if(dir->paths_size <= 0)
        {
            char *file_name = generate_random_word(FILE_NAME_SIZE);
            char *path = form_request_path(dir->dir_name, file_name, request->dir_index);
            strcpy(request->op, OP[1]);
            request->op_path = path;
        }
        else
        {
            char *file = get_random_file(storage->dirs[request->dir_index]);
            char *text = generate_random_word(WORD_SIZE);
            char *path = form_request_path(dir->dir_name, file, request->dir_index);
            request->op_path = path;
            request->arg = text;
        }
    }
    else if (strcmp(request->op, OP[5]) == 0) //mkdir
    {
        directory_t *parent_dir = get_random_directory(request);
        char *dir_name = generate_random_word(DIR_NAME_SIZE);
        char *path = form_request_path(parent_dir->dir_name, dir_name, request->dir_index);
        request->op_path = path;
    }
	request->req_id = ++req_id;
	return request;
}

void *consumer(void *arg)
{
	int id = *((int *) arg);
	while (1)
	{
		pthread_mutex_lock(&mutex);
        while (fifo->full != 1)
            pthread_cond_wait(&cond_consumer, &mutex);
        request_t *req = fifo_dequeue(fifo);
		//fprintf(stdout, "EXEC %s %s \n", req->op, req->op_path);
        pthread_mutex_unlock(&mutex);
        if (fifo_count(fifo) <= 0)
        {
            fifo->full = 0;
            pthread_cond_broadcast(&cond_producer);
        }
		if(req)
        	spawn_process(req, id);
			sleep(1);

	}
}

void *producer(void *arg)
{
	while (1)
	{
		pthread_mutex_lock( &mutex);
        while (fifo->full != 0 )
            pthread_cond_wait(&cond_producer,  &mutex);
        request_t *req = generate_random_request();
        fifo_enqueue(fifo, req);
        
        if (fifo_count(fifo) >= MAX_QUEUE_SIZE - 1)
        {
            fifo->full = 1;
            pthread_cond_broadcast(&cond_consumer);
        }
		pthread_mutex_unlock(&mutex);
	}
}

void *writer(void *arg)
{
    int prev_resp_count = 0, prev_req_count = 0;
	char *line;
	int requests_total, responses_total;
	double responses_interval, requests_interval;
	while (1)
	{
		sleep(SAMPLE_INTERVAL);
		time_t t;
		time(&t);
		struct tm *lt;
		struct timeval tmnow;
		gettimeofday(&tmnow, NULL);
		lt = localtime(&t);
		line = calloc(150 , sizeof(char));

		pthread_mutex_lock(&responses_mutex);
		pthread_mutex_lock(&requests_mutex);
		responses_total = responses_count;
		requests_total = requests_count;
		pthread_mutex_unlock(&requests_mutex);
		pthread_mutex_unlock(&responses_mutex);
		responses_interval = ((double)responses_total - prev_resp_count) / SAMPLE_INTERVAL;
		prev_resp_count = responses_total;
		
		
		requests_interval = ( (double) requests_total - prev_req_count) / SAMPLE_INTERVAL;
		prev_req_count = requests_total;
		
		prev_req_count = requests_count;
		char buf[32];
		buf[strftime(buf, sizeof(buf), "%H:%M:%S", lt)] = '\0';
		double mean_response = ( 1 / (double) responses_interval ) * 1000;
		sprintf(line, "%s Responses/requests per sec %.1f/%.1f. Mean Response Time: %.3f ms. Total: %d/%d",
					 buf, responses_interval, requests_interval, mean_response,responses_count, requests_count);
		
		fprintf(results_fp,"%s\n",line);
		fflush(results_fp);
		memset(line, 0, sizeof(char));
	}
}

int main(int argc, char const *argv[])
{
	if(argc < 2)
	{
		fprintf(stderr, " Must pass output file name in argv[1]\n");
		return 0;
	}
	srand(time(NULL));
	storage = storage_init();
	fifo = fifo_init(fifo);	
	results_fp = fopen(argv[1],"w");
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
	pthread_cond_init(&cond_consumer, NULL);
	pthread_cond_init(&cond_producer, NULL);
	//pthread_create(&producer_th, NULL, producer, NULL);
	for (int i = 0; i < PROD_THREAD_NUM; i++)
		pthread_create(&producer_th[i], &attr, producer, (void *) &i);
	for (int i = 0; i < CONS_THREAD_NUM; i++)
		pthread_create(&consumer_th[i], &attr, consumer, (void *) &i);
	pthread_create(&writer_th, NULL, writer, NULL);
	pthread_join(writer_th,NULL);
	return 0;
}
