#include <stdlib.h>
#include <pthread.h>
#include "test_client.h"
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <time.h>

int requests_count = 0;
int responses_count = 0;
FILE *results_fp;
file_storage_t *directories[DIRS_SIZE];
pthread_t producer_th[PROD_THREAD_NUM];
pthread_t consumer_th[CONS_THREAD_NUM];
pthread_t writer_th;
pthread_mutex_t mutex[CONS_THREAD_NUM];
pthread_mutex_t responses_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t queue_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t requests_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t dir_creation_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond_consumer[CONS_THREAD_NUM];
pthread_cond_t cond_producer[CONS_THREAD_NUM];
pthread_attr_t attr;
//static fifo_t *fifo;
fifo_t *thread_queues[CONS_THREAD_NUM];
int requests[PROD_THREAD_NUM];
int responses[PROD_THREAD_NUM];
double timers[PROD_THREAD_NUM];
int req_id = 0;
int dirs_count = 0;

int prev_resp_count = 0, prev_req_count = 0;
	char *line;
	int requests_total, responses_total, responses_interval, requests_interval;

int op_weighted_rand()
{
	double rand_value = (double)rand() / (double)RAND_MAX;
	if(rand_value <= 0.35)
		return 0; // ls
	else if(rand_value > 0.35 && rand_value <= 0.4)
		return 2; // rm
	else if(rand_value > 0.4 && rand_value <= 0.6)
		return 5; // mkdir
	else if(rand_value > 0.6 && rand_value <= 0.75)
		return 1; // mkfile
	else if(rand_value > 0.75 && rand_value <= 0.90)
		return 3; // read
	else
		return 4; // write
}

void execute_request_result(request_t *req, int id)
{
	printf("%d received response from %d \n", id,req->req_id);
	if (strcmp(req->op, OP[1]) == 0)
	{
		char *file_name = get_file_name(req);
		fs_append(directories[req->dir_index], file_name);
	}/*
	else if (strcmp(req->op, OP[2]) == 0)
	{
		char *file_name = get_file_name(req);
		fs_delete(directories[req->dir_index], file_name);
	}*/
	else if(strcmp(req->op,OP[5]) == 0)
	{ 
		if(dirs_count < DIRS_SIZE)
		{
			//pthread_mutex_lock(&dir_creation_mutex);
			dirs_count++;
			directories[dirs_count] = fs_init(req->op_path, dirs_count);
			//pthread_mutex_unlock(&dir_creation_mutex);
		}
	}
	
}

void spawn_process(request_t *req, int id)
{
	pthread_mutex_lock(&requests_mutex);
	requests_count++;
	pthread_mutex_unlock(&requests_mutex);
	pid_t fpid = fork();
	int status;
	if (fpid == 0)
	{
		execute(req);
		_exit(111);
	}
	if(WAIT_FOR_RESPONSE == 1)
	{
		if (fpid > 0)
		{
			if (waitpid(fpid, &status, 0) > 0)
			{
				
				if (WIFEXITED(status) && !WEXITSTATUS(status))
				{
					pthread_mutex_lock(&responses_mutex);
					responses_count++;
					pthread_mutex_unlock(&responses_mutex);
					execute_request_result(req, id);
				}
				else if (WIFEXITED(status) && WEXITSTATUS(status))
					if (WEXITSTATUS(status) == 111)
						perror("execl");

			}
			free(req);
		}
	}
	else
	{
		execute_request_result(req, id);
	}
	
}

char *generate_random_word(int length)
{
	int word_length = rand() % length + 5;
	char *word = calloc(word_length , sizeof(*word));
	int i;
	for (i = 0; i < word_length; i++)
		word[i] = 'a' + (rand() % 25);
	word[word_length-1] = '\0';
	return word;
}

char *get_random_file(file_storage_t *fs)
{
	if(fs->paths_size <= 0)
		return generate_random_word(FILE_NAME_SIZE);
	else
		return fs_get_file(fs);
}

char *get_random_directory(request_t *req)
{
	//char *dir;
	int dir_index = 0;
	if (dirs_count > 0)
		dir_index = rand() % dirs_count;
	req->dir_index = dir_index;
	return directories[dir_index]->dir_name;
}

char *form_request_path(char *dir, char *file, int dir_index)
{
	size_t dir_len = strlen(dir);
	size_t file_len = strlen(file) + 1;
	char *path = NULL;
	if(dir_len > 1)
	{
		int backslash_len = strlen("/");
		path = calloc( dir_len + file_len + backslash_len ,sizeof(*path));
		strcat(path, dir);
		strcat(path, "/");
		strcat(path, file);
	}
	else
	{
		path = calloc( dir_len + file_len , sizeof(*path));
		strcat(path, dir);
		strcat(path, file);
	}
	return path;
}

request_t *generate_random_request()
{
	request_t *request = (request_t *)malloc(sizeof(request_t));
	request->op = NULL;
	request->op_path = NULL;
	request->arg = NULL;
	request->dir_index = -1;
	int request_type = op_weighted_rand();
	char *op = calloc( strlen(OP[request_type]) + 1, sizeof(*op));
	strcpy(op, OP[request_type]);
	request-> op = op;
	if(strcmp(request->op, OP[0]) == 0) //ls
	{
		int dir_index;
		if(dirs_count > 0)
			dir_index = rand() % dirs_count;
		else
			dir_index = dirs_count;	
		request-> op_path = directories[dir_index]->dir_name;
	}
	else if (strcmp(request->op, OP[1]) == 0 ) //mkfile
	{
		char *file_name = generate_random_word(FILE_NAME_SIZE);
		char *dir = get_random_directory(request);
		char *path = form_request_path(dir, file_name, request->dir_index);
		request->op_path = path;
	}	
	else if (strcmp(request->op, OP[2]) == 0 || strcmp(request->op, OP[3]) == 0 ) // read/rm
	{
		char *dir = get_random_directory(request);
		char *file = get_random_file(directories[request->dir_index]);
		char *path = form_request_path(dir, file, request->dir_index);
		request->op_path = path;
	}
	else if (strcmp(request->op, OP[4]) == 0) //write
	{
		char *dir = get_random_directory(request);
		char *file = get_random_file(directories[request->dir_index]);
		char *text = generate_random_word(WORD_SIZE);
		char *path = form_request_path(dir, file, request->dir_index);
		request->op_path = path;
		request->arg = text;
	}
	else if(strcmp(request->op, OP[5]) == 0) //mkdir
	{
		char *parent_dir = get_random_directory(request);
		char *dir_name = generate_random_word(DIR_NAME_SIZE);
		char *path = form_request_path(parent_dir, dir_name, request->dir_index);
		request->op_path = path;		
	}
	request->req_id = ++req_id;
	return request;
}

void *consumer(void *arg)
{
	int index = *((int *) arg);
    printf("%d \n",index);
	while (1)
	{
		//pthread_mutex_lock(&mutex);
		while (fifo_count(thread_queues[index]) <= 0 )
			pthread_cond_wait(&cond_consumer[index], &mutex[index]);
		request_t *req = fifo_dequeue(thread_queues[index]);
		//printf("Executing : %s %s %d Queue Size %ld \n", req->op, req->op_path, req->req_id, fifo_count(fifo));		
		//pthread_mutex_unlock(&mutex);
        if(fifo_count(thread_queues[index]) <= 0 )
            pthread_cond_signal(&cond_producer[index]);
		if (req)
			spawn_process(req, index);
		sleep(1);
	}
}

void *producer(void *arg)
{
    int index = *((int *) arg);
    printf("Producer %d \n",index);

	while (1)
	{
		//pthread_mutex_lock(&mutex);
		//request_t *req;
		while (fifo_count(thread_queues[index]) >= MAX_QUEUE_SIZE)
			pthread_cond_wait(&cond_producer[index], &mutex[index]);
		
		//req = generate_random_request();
		fifo_enqueue(thread_queues[index], generate_random_request());
		/*if(fifo_count(fifo) <= 0)
		{
			fifo_free(&fifo,1);
			fifo=fifo_init(fifo);
		}*/
		//execute_request_result(req);
		if(fifo_count(thread_queues[index]) >= MAX_QUEUE_SIZE)
			pthread_cond_signal(&cond_consumer[index]);
		//pthread_mutex_unlock(&mutex);
		
		
	}
}

void *writer(void *arg)
{
    
	while (1)
	{
		sleep(SAMPLE_INTERVAL);
		time_t t;
		time(&t);
		struct tm *lt;
		struct timeval tmnow;
		gettimeofday(&tmnow, NULL);
		lt = localtime(&t);
		line = calloc(150 , sizeof(char));

		pthread_mutex_lock(&requests_mutex);
        pthread_mutex_lock(&responses_mutex);
		responses_total = responses_count;
		requests_total = requests_count;
		pthread_mutex_unlock(&responses_mutex);
        pthread_mutex_unlock(&requests_mutex);
		responses_interval = (responses_total - prev_resp_count) / SAMPLE_INTERVAL;
		prev_resp_count = responses_total;
		
		
		
		
		requests_interval = (requests_total - prev_req_count) / SAMPLE_INTERVAL;
		prev_req_count = requests_total;
		
		prev_req_count = requests_count;
		char buf[32];
		buf[strftime(buf, sizeof(buf), "%H:%M:%S", lt)] = '\0';
		double mean_response = ( 1 / (double) responses_interval ) * 1000;
		sprintf(line, "%s Responses/requests per sec %d/%d. Mean Response Time: %.3f ms. Total: %d/%d",
					 buf, responses_interval, requests_interval, mean_response,responses_count, requests_count);
		
		fprintf(results_fp,"%s\n",line);
		fflush(results_fp);
		memset(line, 0, sizeof(char));
	}
}

int main(int argc, char const *argv[])
{
	if(argc < 2)
	{
		fprintf(stderr, " Must pass output file name in argv[1]\n");
		return 0;
	}
	srand(time(NULL));

	directories[0] = fs_init("/", 0);

//	fifo = fifo_init(fifo);	
	results_fp = fopen(argv[1],"w");
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
	for(int i = 0; i < PROD_THREAD_NUM; i++)
    {
        thread_queues[i] = fifo_init();
        
    }


	pthread_create(&writer_th, NULL, writer, NULL);
	pthread_join(writer_th,NULL);
	return 0;
}