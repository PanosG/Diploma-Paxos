#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <signal.h>
#include "fifo.h"
#include "file_storage.h"

#define WAIT_FOR_RESPONSE	1
#define SAMPLE_INTERVAL		1
#define PROD_THREAD_NUM		1
#define CONS_THREAD_NUM		2
const char *CFC_PATH = "./../paxos_source/file-store/cfc";
const char *CFC = "cfc";
const char *SERVER = "localhost";
const char *UDP_PORT = "7777";
const char *OP[] = {"ls", "mkfile", "rm", "read", "write", "mkdir"};
const char *MKDIR = "mkdir";

int execute(request_t *req)
{
	int res;
	if (req->arg != NULL)
		res = execl(CFC_PATH, CFC, SERVER, UDP_PORT, req->op, req->op_path, req->arg, NULL);
	else
		res = execl(CFC_PATH, CFC, SERVER, UDP_PORT, req->op, req->op_path, NULL);
	perror("execl");
	return res;
}

char *get_file_name(request_t *req)
{
	char *delim = "/";
	char *tokens = strtok(req->op_path, delim);
	char *file_name = NULL;
	while(tokens != NULL)
	{
		file_name = tokens;
		tokens = strtok(NULL, delim);
	}
	return file_name;
}