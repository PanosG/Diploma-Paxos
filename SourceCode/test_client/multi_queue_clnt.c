#include <stdlib.h>
#include <pthread.h>
#include "test_client.h"
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <time.h>

int requests_count = 0;
int responses_count = 0;
FILE *results_fp;

storage_t *th_storage[PROD_THREAD_NUM];

pthread_t writer_th;
pthread_mutex_t dir_creation_mutex[PROD_THREAD_NUM];
pthread_attr_t attr;
static fifo_t *fifo;

static fifo_t *thread_queues[PROD_THREAD_NUM];
int requests[PROD_THREAD_NUM];
int responses[PROD_THREAD_NUM];
int req_ids[PROD_THREAD_NUM];
clock_t timers[PROD_THREAD_NUM];
pthread_t producer_th[PROD_THREAD_NUM];
pthread_t consumer_th[CONS_THREAD_NUM];
int total_responses = 0;
int total_requests = 0;

int prev_resp_count = 0, prev_req_count = 0;
char *line;

int op_weighted_rand()
{
    double rand_value = (double)rand() / (double)RAND_MAX;
    /*if (rand_value <= 0.3)
        return 0; // ls
    else if (rand_value > 0.35 && rand_value <= 0.4)
        return 2; // rm*/
    if (rand_value <= 0.15)
        return 5; // mkdir
    else if (rand_value > 0.15 && rand_value <= 0.3)
        return 1; // mkfile
    else if (rand_value > 0.3 && rand_value <= 0.65)
        return 3; // read
    else
        return 4; // write
}

void execute_request_result(request_t *req, int id)
{

    if (strcmp(req->op, OP[1]) == 0)
    {
        char *file_name = get_file_name(req);
        directory_t *dir = th_storage[id]->dirs[req->dir_index];
        dir_append(dir, file_name);
    } /*
	else if (strcmp(req->op, OP[2]) == 0)
	{
		char *file_name = get_file_name(req);
		fs_delete(directories[req->dir_index], file_name);
	}*/
    else if (strcmp(req->op, OP[5]) == 0)
    {
        pthread_mutex_lock(th_storage[id]->mutex);
        if (th_storage[id]->dir_count < DIRS_SIZE)
        {
            int index = ++th_storage[id]->dir_count;
            th_storage[id]->dirs[index] = dir_init(req->op_path, index);
        }
        pthread_mutex_unlock(th_storage[id]->mutex);
    }
}

void spawn_process(request_t *req, int id)
{
    pid_t fpid = fork();
    int status;
    if (fpid == 0)
    {
        execute(req);
        _exit(111);
    }
    if(WAIT_FOR_RESPONSE == 1)
    {
        if (fpid > 0)
        {
            pthread_mutex_lock(thread_queues[id]->requests_mutex);
            ++thread_queues[id]->requests;
            pthread_mutex_unlock(thread_queues[id]->requests_mutex);
            if (waitpid(fpid, &status, 0) > 0)
            {

                if (WIFEXITED(status) && !WEXITSTATUS(status))
                {
                    execute_request_result(req, id);
                }
                else if (WIFEXITED(status) && WEXITSTATUS(status))
                {
                    if (WEXITSTATUS(status) == 111)
                        fprintf(stderr, " EXIT 111 \n");
                }
                
                
                pthread_mutex_lock(thread_queues[id]->responses_mutex);
                ++thread_queues[id]->responses;
                pthread_mutex_unlock(thread_queues[id]->responses_mutex);
            }
            free(req);
        }
    }
    else
        execute_request_result(req, id);
    //sleep(2);
}

char *generate_random_word(int length)
{
    int word_length = rand() % length + 5;
    char *word = calloc(word_length, sizeof(*word));
    int i;
    for (i = 0; i < word_length; i++)
        word[i] = 'a' + (rand() % 25);
    word[word_length - 1] = '\0';
    return word;
}

char *get_random_file(directory_t *dir)
{
    if (dir->paths_size <= 0)
        return generate_random_word(FILE_NAME_SIZE);
    else
        return dir_get_file(dir);
}

directory_t *get_random_directory(request_t *req, int th_id)
{
    //char *dir;
    int dir_index = 0;
    int dirs_count = th_storage[th_id]->dir_count;
    if (dirs_count > 0)
        dir_index = rand() % dirs_count;
    req->dir_index = dir_index;
    //fprintf(stderr,"%ld %d %d\n", directories[dir_index]->paths_size, dir_index, dirs_count);
    return th_storage[th_id]->dirs[dir_index];
}

char *form_request_path(char *dir, char *file, int dir_index)
{
    size_t dir_len = strlen(dir);
    size_t file_len = strlen(file) + 1;
    char *path = NULL;
    if (dir_len > 1)
    {
        int backslash_len = strlen("/");
        path = calloc(dir_len + file_len + backslash_len, sizeof(*path));
        strcat(path, dir);
        strcat(path, "/");
        strcat(path, file);
    }
    else
    {
        path = calloc(dir_len + file_len, sizeof(*path));
        strcat(path, dir);
        strcat(path, file);
    }
    return path;
}

request_t *generate_random_request(int th_id)
{
    request_t *request = (request_t *)malloc(sizeof(request_t));
    request->op = NULL;
    request->op_path = NULL;
    request->arg = NULL;
    request->dir_index = -1;
    int request_type = op_weighted_rand();
    char *op = calloc(strlen(OP[request_type]) + 1, sizeof(*op));
    strcpy(op, OP[request_type]);
    request->op = op;
    if (strcmp(request->op, OP[0]) == 0) //ls
    {
        directory_t *dir = get_random_directory(request, th_id);
        request->op_path = dir->dir_name;
    }
    else if (strcmp(request->op, OP[1]) == 0) //mkfile
    {
        char *file_name = generate_random_word(FILE_NAME_SIZE);
        directory_t *dir = get_random_directory(request, th_id);
        char *path = form_request_path(dir->dir_name, file_name, request->dir_index);
        request->op_path = path;
    }
    else if (strcmp(request->op, OP[2]) == 0 || strcmp(request->op, OP[3]) == 0) // read/rm
    {
        directory_t *dir = get_random_directory(request, th_id);
        if(dir->paths_size <= 0)
        {
            char *file_name = generate_random_word(FILE_NAME_SIZE);
            char *path = form_request_path(dir->dir_name, file_name, request->dir_index);
            strcpy(request->op, OP[1]);
            request->op_path = path;
        }
        else
        {
            char *file = get_random_file(th_storage[th_id]->dirs[request->dir_index]);
            char *path = form_request_path(dir->dir_name, file, request->dir_index);
            request->op_path = path;
        }
    }
    else if (strcmp(request->op, OP[4]) == 0) //write
    {
        directory_t *dir = get_random_directory(request, th_id);
        if(dir->paths_size <= 0)
        {
            char *file_name = generate_random_word(FILE_NAME_SIZE);
            char *path = form_request_path(dir->dir_name, file_name, request->dir_index);
            strcpy(request->op, OP[1]);
            request->op_path = path;
        }
        else
        {
            char *file = get_random_file(th_storage[th_id]->dirs[request->dir_index]);
            char *text = generate_random_word(WORD_SIZE);
            char *path = form_request_path(dir->dir_name, file, request->dir_index);
            request->op_path = path;
            request->arg = text;
        }
    }
    else if (strcmp(request->op, OP[5]) == 0) //mkdir
    {
        directory_t *parent_dir = get_random_directory(request, th_id);
        char *dir_name = generate_random_word(DIR_NAME_SIZE);
        char *path = form_request_path(parent_dir->dir_name, dir_name, request->dir_index);
        request->op_path = path;
    }
    request->req_id = ++req_ids[th_id];
    return request;
}

void *consumer(void *arg)
{
    int id = *((int *)arg);
    //sleep(1);
    while (1)
    {
        pthread_mutex_lock(thread_queues[id]->mutex);
        while (thread_queues[id]->full != 1)
            pthread_cond_wait(thread_queues[id]->consumer_cond, thread_queues[id]->mutex);
        request_t *req = fifo_dequeue(thread_queues[id]);
        pthread_mutex_unlock(thread_queues[id]->mutex);
        if (fifo_count(thread_queues[id]) <= 0)
        {
            thread_queues[id]->full = 0;
            pthread_cond_signal(thread_queues[id]->producer_cond);
        }
        if(req)
        {
            fprintf(stderr, " EXEC %s %s \n", req->op, req->op_path);
            spawn_process(req, id);
            sleep(1);
        }
    }
}

void *producer(void *arg)
{
    int id = *((int *)arg);
    sleep(1);
    while (1)
    {
        pthread_mutex_lock(thread_queues[id]->mutex);
        while (thread_queues[id]->full != 0 )
            pthread_cond_wait(thread_queues[id]->producer_cond, thread_queues[id]->mutex);
        request_t *req = generate_random_request(id);
        fifo_enqueue(thread_queues[id], req);
        pthread_mutex_unlock(thread_queues[id]->mutex);
        if (fifo_count(thread_queues[id]) >= MAX_QUEUE_SIZE - 1)
        {
            thread_queues[id]->full = 1;
            pthread_cond_broadcast(thread_queues[id]->consumer_cond);
        }
        
    }
}

void write_header()
{
    time_t t;
    time(&t);
    struct tm *lt;
    struct timeval tmnow;
    gettimeofday(&tmnow, NULL);
    lt = localtime(&t);
    line = calloc(150, sizeof(char));
    char buf[10];
    buf[strftime(buf, sizeof(buf), "%H:%M:%S", lt)] = '\0';
    sprintf(line, "%s,%s,%s,%s,%s,%s",
            buf, "Responses/sec", "Requests/sec", "Mean Response Time", "Total Responses", "Total Requests");
    fprintf(results_fp, "%s\n", line);
    fflush(results_fp);
}

void *writer(void *arg)
{
    int requests_total = 0;
    int responses_total = 0;
    double prev_req_count = 0;
    double prev_resp_count = 0;
    double responses_interval;
    double requests_interval;
    int secs = 0;
    write_header();
    sleep(1);
    while (1)
    {
        sleep(SAMPLE_INTERVAL);
        ++secs;
        for (int id = 0; id < PROD_THREAD_NUM; id++)
        {
            pthread_mutex_lock(thread_queues[id]->mutex);
            pthread_mutex_lock(thread_queues[id]->requests_mutex);
            pthread_mutex_lock(thread_queues[id]->responses_mutex);
            requests_total += thread_queues[id]->requests;
            
            responses_total += thread_queues[id]->responses;
            pthread_mutex_unlock(thread_queues[id]->responses_mutex);
            pthread_mutex_unlock(thread_queues[id]->requests_mutex);
            pthread_mutex_unlock(thread_queues[id]->mutex);
        }
        time_t t;
        time(&t);
        struct tm *lt;
        struct timeval tmnow;
        gettimeofday(&tmnow, NULL);
        lt = localtime(&t);
        line = calloc(150, sizeof(char));

        responses_interval =  ( (double) responses_total - prev_resp_count) / (double) SAMPLE_INTERVAL;
        prev_resp_count = responses_total;

        requests_interval = (  (double) requests_total - prev_req_count) / (double) SAMPLE_INTERVAL;
        prev_req_count = requests_total;

        char buf[10];
        buf[strftime(buf, sizeof(buf), "%S", lt)] = '\0';
        double mean_response = (1 / (double)responses_interval) * 1000;
        sprintf(line, "%d,%.1f,%.1f,%.3f,%d,%d",
                secs, responses_interval, requests_interval, mean_response, responses_total, requests_total);

        fprintf(results_fp, "%s\n", line);
        fflush(results_fp);
        memset(line, 0, sizeof(char));
        requests_total = 0;
        responses_total = 0;
        
    }
}

int main(int argc, char const *argv[])
{
    if (argc < 2)
    {
        fprintf(stderr, " Must pass output file name in argv[1]\n");
        return 0;
    }
    srand(time(NULL));

    
    fifo = fifo_init(fifo);
    results_fp = fopen(argv[1], "w");
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
    for (int i = 0; i < PROD_THREAD_NUM; i++)
    {
        th_storage[i] = storage_init();
        
        thread_queues[i] = fifo_init();
        responses[i] = -1;
        requests[i] = -1;
        timers[i] = -1;
        req_ids[i] = 0;
        pthread_create(&producer_th[i], &attr, producer, (void *)&i);
        int index = i*(CONS_THREAD_NUM/PROD_THREAD_NUM);
        for(int j = 0; j < (CONS_THREAD_NUM/PROD_THREAD_NUM) ; j++)
            pthread_create(&consumer_th[index+j], &attr, consumer, (void *)&i);
    }
    sleep(1);
    pthread_create(&writer_th, NULL, writer, NULL);
    pthread_join(writer_th,NULL);
    return 0;
}