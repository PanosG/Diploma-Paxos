#ifndef INC_05_ENC_DEC_FIFO_H
#define INC_05_ENC_DEC_FIFO_H
#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <pthread.h>
#include "queue.h"

struct list_node {
    struct list_node *prev;
    struct list_node *next;
    request_t *req;
};

typedef struct FIFO {
    struct list_node *first;
    struct list_node *last;
    size_t count;
    pthread_mutex_t *mutex;
    pthread_mutex_t *requests_mutex;
    pthread_mutex_t *responses_mutex;
    pthread_cond_t *producer_cond;
    pthread_cond_t *consumer_cond;
    int full;
    int requests;
    int responses;
    clock_t time;
    size_t (*count_mutex)(struct FIFO*);
    void (*enqueue)(struct FIFO*, request_t*);
    request_t* (*dequeue)(struct FIFO*);
    void (*free)(struct FIFO**, bool);
} fifo_t;

struct thread_bus {
    fifo_t *input;
    fifo_t *output;
    bool kill;
};

fifo_t *fifo_init();
void fifo_enqueue(fifo_t *queue, request_t *req);
request_t *fifo_dequeue(fifo_t *queue);
void fifo_free(fifo_t **queue, bool free_data);
size_t fifo_count(fifo_t *queue);
int fifo_responses(fifo_t *queue);
int fifo_requests(fifo_t *queue);
void fifo_reset(fifo_t *queue);
#endif //INC_05_ENC_DEC_FIFO_H