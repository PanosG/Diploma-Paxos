#include "fifo.h"

fifo_t *fifo_init() {
    fifo_t *queue = calloc(1,sizeof(fifo_t)); 
    queue->first = NULL;
    queue->mutex = calloc(1,sizeof(pthread_mutex_t)); 
    queue->requests_mutex  = calloc(1,sizeof(pthread_mutex_t)); 
    queue->responses_mutex  = calloc(1,sizeof(pthread_mutex_t)); 
    queue->responses_mutex  = calloc(1,sizeof(pthread_mutex_t)); 
    queue->consumer_cond  = calloc(1,sizeof(pthread_cond_t)); 
    queue->producer_cond  = calloc(1,sizeof(pthread_cond_t)); 
    pthread_mutex_init(queue->mutex, NULL); 
    pthread_mutex_init(queue->requests_mutex, NULL); 
    pthread_mutex_init(queue->responses_mutex, NULL); 
    pthread_cond_init(queue->consumer_cond, NULL);
    pthread_cond_init(queue->producer_cond, NULL);
    queue->count = 0; 
    queue->full = 0;
    queue->requests = 0;
    queue->responses = 0;
    queue->count_mutex = fifo_count;
    queue->enqueue = fifo_enqueue;
    queue->dequeue = fifo_dequeue;
    queue->free = fifo_free;

    return queue;
}

/*
 * Enqueues into FIFO
 */
void fifo_enqueue(fifo_t *queue, request_t *req)
{
    struct list_node *new = (struct list_node *)malloc(sizeof(struct list_node));
    new->req = req; 
    if (queue->count == 0)
    {
        queue->first = queue->last = new;
        ++queue->count;
        return;
    }

    queue->last->next = new;
    new->prev = queue->last;
    queue->last = new;
    ++queue->count;
}

/*
 * Dequeue from FIFO
 */
request_t *fifo_dequeue(fifo_t *queue) {
    if (queue->first == NULL) {
        return NULL;
    }

    struct list_node *first = queue->first;
    if(first->req)
    {
        queue->first = first->next;
        first->prev = NULL;
    }
    else
    {
        printf("Queue->First is NUL\n");
        queue->first = queue->last;
        queue->count = 0;
        return queue->first->req;
    }

    request_t *req = first->req;
    free(first);

    --queue->count;
    if(queue->count <= 0)
        queue->time = clock() - queue->time;
    if(queue->count >= MAX_QUEUE_SIZE)
        queue->time = clock();
    return req;
}

/*
 * Free FIFO
 */
void fifo_free(fifo_t **queue, bool free_data) {
    pthread_mutex_lock((*queue)->mutex); 

    struct list_node *index;
    while ((index = (*queue)->first) != NULL) {
        if((*queue)-> count > 0)
        {
            (*queue)->first = (*queue)->first->next;
            if (free_data) {
                free(index->req);
                index->req = NULL;
            }
            free(index);
        }
        else
        {
            (*queue)->first = NULL;
        }
    }
    pthread_mutex_unlock((*queue)->mutex);
    pthread_mutex_destroy(((*queue)->mutex));
    free((*queue)->mutex);
    free(*queue);
    *queue = NULL;
}

size_t fifo_count(fifo_t *queue) {
   // pthread_mutex_lock(queue->mutex);
    size_t res = queue->count; 
   // pthread_mutex_unlock(queue->mutex);
    return res;
}

int fifo_respones(fifo_t *queue)
{
   // pthread_mutex_lock(queue->mutex);
    int res = queue->responses; 
   // pthread_mutex_unlock(queue->mutex);
    return res;
}

int fifo_requests(fifo_t *queue)
{
   // pthread_mutex_lock(queue->mutex);
    int res = queue->requests; 
   // pthread_mutex_unlock(queue->mutex);
    return res;
}

void fifo_reset(fifo_t *queue)
{
    queue->requests = 0;
    queue->responses = 0;
}