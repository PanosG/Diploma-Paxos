#ifndef QUEUE_H
#define QUEUE_H
#include <pthread.h>
 
#define MAX_QUEUE_SIZE  2000

typedef struct request
{
	char *op_path;
	char *arg;
	char *op;
	int req_id;
	int dir_index;
} request_t;

#endif