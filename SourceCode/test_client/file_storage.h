#ifndef FILE_STORAGE_H
#define FILE_STORAGE_H
#include <pthread.h>
#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#define DIRS_SIZE   5000
#define WORD_SIZE   200
#define MAX_FILES   1000
#define FILE_NAME_SIZE 35
#define DIR_NAME_SIZE 25



typedef struct directory
{
    char *dir_name;
    char *file_paths[MAX_FILES];
    size_t paths_size;
    int dir_index;
    int current_file;
    int reverse;
    size_t file_count;
    pthread_mutex_t *mutex;
}directory_t;

typedef struct storage
{
    int dir_count;
    directory_t *dirs[DIRS_SIZE];
    pthread_mutex_t *mutex;

}storage_t;


directory_t *dir_init(char *dir_name, int dir_index);
void dir_append(directory_t *dir, char *file_name);
void dir_delete(directory_t *dir, char *file_name);
int dir_count(directory_t *dir);
void dir_free(directory_t **dir, bool free_data);
char *dir_get_file(directory_t *dir);

storage_t *storage_init();



#endif