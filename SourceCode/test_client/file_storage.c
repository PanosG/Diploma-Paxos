#include "file_storage.h"

directory_t *dir_init(char *dir_name, int dir_index)
{
    directory_t *dir = (directory_t *)malloc(sizeof(directory_t)); 
    dir->dir_name = dir_name;
    dir->dir_index = dir_index;
    dir->paths_size = 0;
    dir->current_file = 0;
    dir->reverse = 0;
    dir->mutex = calloc(1,sizeof(pthread_mutex_t));
    pthread_mutex_init(dir->mutex, NULL); 
    return dir;
}

storage_t *storage_init()
{
    storage_t *storage = (storage_t *)malloc(sizeof(storage_t)); 
    storage->dir_count = 0;
    storage->dirs[0] = dir_init("/", 0);
    storage->mutex = calloc(1,sizeof(pthread_mutex_t));
    return storage;
}

void dir_append(directory_t *dir, char *file_name)
{
    pthread_mutex_lock(dir->mutex);
    if( dir->paths_size < MAX_FILES)
    {
        dir->file_paths[dir->paths_size] = file_name;
        dir->paths_size++;
        ++dir->file_count;
    }    
    pthread_mutex_unlock(dir->mutex);
}

void dir_delete(directory_t *dir, char *file_name)
{
    if(dir->paths_size == 0) return;
    pthread_mutex_lock(dir->mutex);
    for(int i = 0; i < dir->paths_size; i++)
    {
        if (dir->file_paths[i] != NULL)
        {
            if( strcmp(dir->file_paths[i], file_name) == 0)
            {
                dir->file_paths[i] = NULL;
                break;
            }
        }
    }
    --dir->file_count;
    pthread_mutex_unlock(dir->mutex);
}

void dir_free(directory_t **dir, bool free_data)
{
    for(int i = 0; i< (*dir)->paths_size; i++)
        free((*dir)->file_paths[i]);
    free((*dir)->dir_name);
    pthread_mutex_unlock((*dir)->mutex);
    pthread_mutex_destroy((*dir)->mutex);
    free((*dir)->mutex);
    free(*dir);

}

int dir_count(directory_t *dir)
{
    pthread_mutex_lock(dir->mutex);
    size_t count = dir->file_count;
    pthread_mutex_unlock(dir->mutex);
    return count;
}

char *dir_get_file(directory_t *dir)
{
    char *file_name;
    if(dir->current_file == MAX_FILES -1 && dir->reverse == 0)
    {
        dir->reverse = 1;
        dir->current_file = MAX_FILES - 1 ;
        file_name = dir->file_paths[dir->current_file];

    }
    else if(dir->current_file == MAX_FILES-1 && dir->reverse == 1)
    {
        file_name = dir->file_paths[dir->current_file];
        --dir->current_file;
    }
    else if(dir->current_file == 0 && dir->reverse == 1)
    {
        dir->reverse = 0;
        file_name = dir->file_paths[dir->current_file];
        ++dir->current_file;
    }
    else if(dir->current_file == 0 && dir->reverse == 0)
    {   
        file_name = dir->file_paths[dir->current_file];
        ++dir->current_file;
    }
    else 
    {
        if(dir->reverse == 0)
            --dir->current_file;
        else
            ++dir->current_file;
        file_name = dir->file_paths[dir->current_file];
        
    }
    
    return file_name;
}