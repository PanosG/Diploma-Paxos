#include "log_utils.h"

queue_t *log_queue;
pthread_t producer_th;
pthread_t consumer_th[THREAD_NUM];
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond_consumer = PTHREAD_COND_INITIALIZER;
pthread_cond_t cond_producer = PTHREAD_COND_INITIALIZER;
pthread_attr_t attr;
int socket_fd, connection_fd, consume_fd,read_size;
char log_entry[BUF_SIZE];
socklen_t clen;
struct sockaddr_in server_addr,client_addr;
char buffer[BUF_SIZE];

void *consumer(void *arg)
{
    while(1)
    {
        pthread_mutex_lock(&mutex);
        while (log_queue->size == 0)
            pthread_cond_wait(&cond_consumer, &mutex);
        consume_fd = front(log_queue);
        dequeue(log_queue);
        memset(buffer,0,BUF_SIZE);
        read_size = recv(consume_fd,buffer,BUF_SIZE,0 );
        buffer[read_size]  =  '\0';
        write_log(buffer);
        close(consume_fd);
        pthread_cond_signal(&cond_producer);
        pthread_mutex_unlock(&mutex);
    }
}


void *producer(void *arg)
{
    while(1)
    {
        if((connection_fd = accept(socket_fd, (struct sockaddr*)&client_addr, &clen)) == -1)
            perror("accept()");
        pthread_mutex_lock(&mutex);
		while (log_queue->size >= MAX_QUEUE_SIZE)
			pthread_cond_wait(&cond_producer, &mutex);
        enqueue(log_queue, connection_fd);
		pthread_cond_signal(&cond_consumer);
		pthread_mutex_unlock(&mutex);
    }
}

int main(int argc, char **argv)
{
    log_queue = (queue_t *)malloc(sizeof(queue_t));
	init(log_queue);

    if((socket_fd =  socket(AF_INET,  SOCK_STREAM, 0)) == -1)
        perror("(socket()");

    bzero(&server_addr, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    server_addr.sin_port = htons(SERVER_PORT);

    if(bind(socket_fd,(struct  sockaddr*)&server_addr,sizeof(server_addr))  < 0)
    {
        perror("bind()");
        return 1;
    }

    if(listen(socket_fd, MAX_PENDING) < 0)
        perror("listen()");
    fprintf(stderr, "Listening...\n");
    pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
	pthread_cond_init(&cond_consumer, NULL);
	pthread_cond_init(&cond_producer, NULL);
	for(int i=0; i< THREAD_NUM; i++)
	    pthread_create(&consumer_th[i],&attr,consumer, NULL);
    pthread_create(&producer_th, NULL, producer, NULL);
    pthread_join(producer_th, NULL);

    return 0;
}