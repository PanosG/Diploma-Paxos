#include <stdio.h>
#include <stdlib.h>

typedef struct node
{
    int connection_fd;
    struct node *next;
}node_t;

typedef struct queue
 {
    node_t *front;
    node_t *last;
    unsigned int size;
}queue_t;

void init(queue_t *q)
{
    q->front = NULL;
    q->last = NULL;
    q->size = 0;
}

void dequeue(queue_t *q)
{
    q->size--;
    node_t *poped = q->front;
    q->front = q->front->next;
    free(poped);
}

int front(queue_t *q)
{
    return q->front->connection_fd;
}

void enqueue(queue_t *q, int connection_fd)
{
    q->size++;
    if(q->front == NULL)
    {
        q->front = (node_t *)  malloc(sizeof(node_t));
        q->front->connection_fd = connection_fd;
        q->front->next = NULL;
        q->last = q->front;
    }
    else
    {
        q->last->next = (node_t *)  malloc(sizeof(node_t));
        q->last->next->connection_fd = connection_fd;
        q->last->next->next = NULL;
        q->last = q->last->next;
    }
}