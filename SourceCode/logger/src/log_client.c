#include  "log_utils.h"

int main(int argc, char **argv)
{
    struct  sockaddr_in server_addr;
    struct hostent *host_info;
    int socket_fd, numbytes;
    char buff[BUF_SIZE];
    if((host_info = gethostbyname(HOST)) == NULL)
        perror("gethostbyname()");
    if((socket_fd = socket(PF_INET,SOCK_STREAM,0)) == -1)
        perror("socket()");
    bzero(&server_addr, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr = *((struct in_addr*)host_info->h_addr);
    server_addr.sin_port = htons(SERVER_PORT);
    if(connect(socket_fd, (struct sockaddr*) &server_addr, sizeof(server_addr)) == -1)
        perror("connect()");
    write(socket_fd, argv[1], strlen(argv[1]));
    close(socket_fd);
    return 0;
}