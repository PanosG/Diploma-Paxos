#ifndef LOG_H
#define LOG_H

#include <stdio.h>
#include <stdarg.h>
#include <time.h>
#include <pthread.h>

#define LOG_VERSION "0.1.0"
#define LOG_FILE_DIR "/home/cluster/log_files/"
#define LOG_FILE_MAX_LINES 10000
#define BUFFSIZE 1024
#define DATE_DELIMITER ":"
#define CLIENT_PATH "./../logger/src/client"
#define CLIENT_CMD "client"

#define LOG_ACTIVE 1 //Set to 0 to deactivate logger and increase throughput
#define WAIT 1 //Set to 0 to observe true performance without caring for logging consistency

typedef void (*log_LockFn)(void *udata, int lock);

enum { LOG_TRACE, LOG_DEBUG, LOG_INFO, LOG_WARN, LOG_COHORT, LOG_CLIENT_PROXY, LOG_FILE_STORE, LOG_COHORT_ERROR, LOG_FILE_STORE_ERROR, LOG_CLIENT_PROXY_ERROR };

#define log_trace(...) log_log(LOG_TRACE, __FILE__, __LINE__, __VA_ARGS__)
#define log_debug(...) log_log(LOG_DEBUG, __FILE__, __LINE__, __VA_ARGS__)
#define log_info(...)  log_log(LOG_INFO,  __FILE__, __LINE__, __VA_ARGS__)
#define log_warn(...)  log_log(LOG_WARN,  __FILE__, __LINE__, __VA_ARGS__)
#define log_cohort(...) log_log(LOG_COHORT, __FILE__, __LINE__, __VA_ARGS__)
#define log_client_proxy(...) log_log(LOG_CLIENT_PROXY, __FILE__, __LINE__, __VA_ARGS__)
#define log_file_store(...) log_log(LOG_FILE_STORE, __FILE__, __LINE__, __VA_ARGS__)
#define log_client_proxy_error(...) log_log(LOG_CLIENT_PROXY_ERROR, __FILE__, __LINE__, __VA_ARGS__)
#define log_cohort_error(...) log_log(LOG_COHORT_ERROR, __FILE__, __LINE__, __VA_ARGS__)
#define log_file_store_error(...) log_log(LOG_FILE_STORE_ERROR, __FILE__, __LINE__, __VA_ARGS__)

void write_log(char *log_entry);
void create_log_dir(struct tm *lt);
void generate_log(struct tm *lt);
void countlines();
void open_log_file(struct tm *lt);
void log_set_fp();
void log_set_level(int level);
void log_set_quiet(int enable);
void log_log(int level, const char *file, int line, const char *fmt, ...);
void log_request(int level, const char *file, int line, const char *fmt, ...);

#endif
