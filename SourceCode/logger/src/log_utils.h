#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <pthread.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include <netdb.h>
#include "log_queue.h"
#include "log.h"

#define SERVER_PORT		6666
#define BUF_SIZE		500
#define HOST			"localhost"
#define THREAD_NUM		2
#define MAX_PENDING		100
#define MAX_QUEUE_SIZE	100