#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <dirent.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include "log.h"
#include <pthread.h>
#include <sys/wait.h>

static struct
{
	FILE *fp;
	int level;
	int quiet;
} L;

static const char *level_names[] = {
	"TRACE", "DEBUG", "INFO", "WARN", "COHORT", "CLIENT_PROXY", "FILE_STORE", "ERROR_COHORT", "ERROR_FILE_STORE", "ERROR_CLIENT_PROXY"};

static int file_linecount = 0;
static FILE *log_file = NULL;
static char current_log_dir[200];

void log_set_fp(FILE *log_file)
{
	L.fp = log_file;
}

void log_set_level(int level)
{
	L.level = level;
}

void log_set_quiet(int enable)
{
	L.quiet = enable ? 1 : 0;
}

void log_log(int level, const char *file, int line, const char *fmt, ...)
{
	if(LOG_ACTIVE == 0)
		return;
	time_t t;
	time(&t);
	struct tm *lt;
	struct timeval tmnow;
	gettimeofday(&tmnow, NULL);
	lt = localtime(&t);
	if (level < L.level)
		return;
	if (L.quiet == 1 && level < LOG_COHORT_ERROR)
		return;
	va_list args;
	char buf[32], usec_buf[6];
	char *entry = malloc(400*sizeof(entry));
	char log_message[300];
	sprintf(usec_buf,"%.3d",(int) tmnow.tv_usec);
	buf[strftime(buf, sizeof(buf), "%H:%M:%S", lt)] = '\0';
	sprintf(entry, "%s.%s %-5s %s:%d: ", buf,  usec_buf ,level_names[level], file, line);
	va_start(args, fmt);
	vsprintf(log_message,fmt,args);
	va_end(args);
	strcat(entry, log_message);
	int fpid = fork();
	int status;
	if(fpid == 0)
	{
		execl(CLIENT_PATH, CLIENT_CMD, entry, NULL);
		_exit(111);
	}

	if (fpid > 0 && WAIT==1)
		waitpid(fpid, &status, 0);
		
}

void write_log(char *log_entry)
{
	/* Get current time */
	time_t t;
	time(&t);
	struct tm *lt;
	lt = localtime(&t);
	if (  L.fp == NULL || (L.fp != NULL && file_linecount > LOG_FILE_MAX_LINES))
		open_log_file(lt);
	log_set_fp(log_file);
	fprintf(L.fp, "%s\n", log_entry);
	fflush(L.fp);
	file_linecount++;
}

void open_log_file(struct tm *lt)
{
	create_log_dir(lt);
	char file_name[200];
	struct dirent *cont;
	DIR *dr = opendir(current_log_dir);
	if (dr == NULL)
		return;
	while ((cont = readdir(dr)) != NULL)
	{
		int i = 0;
		char *tokens[5];
		memcpy(file_name, cont->d_name, sizeof file_name);
		char *token = strtok(cont->d_name, DATE_DELIMITER);
		while (token != NULL)
		{
			tokens[i++] = token;
			token = strtok(NULL, DATE_DELIMITER);
		}
		tokens[4] = 0;
		if (strcmp(tokens[0], "log_file") != 0)
			continue;
		int min = atoi(tokens[2]);
		int hour = atoi(tokens[1]);
		if (min >= lt->tm_min - 3 && hour == lt->tm_hour)
		{
			memcpy(file_name, strcat(current_log_dir, file_name), sizeof file_name);
			fprintf(stderr, "File Name : %s\n", file_name);
			log_file = fopen(file_name, "r");
			countlines();
			fclose(log_file);
			if (file_linecount >= LOG_FILE_MAX_LINES)
			{
				generate_log(lt);
			}
			else
			{
				log_file = fopen(file_name, "a");
			}
			return;
		}
	}
	generate_log(lt);
}

void countlines()
{
	file_linecount = 0;
	char ch;
	while ((ch = getc(log_file)) != EOF)
	{
		if (ch == '\n')
			file_linecount++;
	}
}

void create_log_dir(struct tm *lt)
{
	struct stat st = {0};
	if (stat(LOG_FILE_DIR, &st) == -1)
		mkdir(LOG_FILE_DIR, 0755);
	// Create/find todays log dir
	char date[200];
	char folder_name[200];
	snprintf(folder_name, sizeof folder_name, "%s", LOG_FILE_DIR);
	date[strftime(date, sizeof(date), "%m-%d", lt)] = '\0';
	strcat(folder_name, date);
	strcat(folder_name, "/");
	fprintf(stderr,"%s\n", folder_name);
	if (stat(folder_name, &st) == -1)
		mkdir(folder_name, 0755);
	memcpy(current_log_dir, folder_name, sizeof current_log_dir);
	fprintf(stderr,"%s\n", current_log_dir);
}

void generate_log(struct tm *lt)
{
	file_linecount = 0;
	char file_name[200];
	file_name[strftime(file_name, sizeof(file_name), "%H:%M:%S", lt)] = '\0';
	char log_file_name[200];
	snprintf(log_file_name, sizeof log_file_name, "%s:%s", "log_file", file_name);
	memcpy(file_name, strcat(current_log_dir, log_file_name), sizeof file_name);
	log_file = fopen(file_name, "a");
}
